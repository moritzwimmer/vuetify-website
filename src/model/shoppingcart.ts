import ShoppingCartEntry from "./shoppingcartEntry";

export default class ShoppingCart {
  [x: string]: any;
  username: string = "";

  items: ShoppingCartEntry[] = [];
  person = {
    name: "",
    vorname: "",
    plz: "",
    adresse: "",
  };

  //addItem-Funktion
  public addItem(item: ShoppingCartEntry) {
    this.items.push(item);
  }

  // static load(): ShoppingCart {
  //   return JSON.parse(localStorage.getItem("cart") || "{}");
  // }

  // static save(cart: ShoppingCart): void {
  //   localStorage.setItem("cart", JSON.stringify(cart));
  // }
}
