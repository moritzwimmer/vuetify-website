export default class ShoppingCartEntry {
  name: string = "";
  id: number | undefined;
  counter: number = 1;

  constructor(name: string, counter: number) {
    this.name = name;
    this.counter = counter;
  }
}
